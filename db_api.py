from pymongo import MongoClient
import json

class DataBase():
	def __init__(self, name, host = "localhost", port = 27017):
		self.client = MongoClient(host, port)
		self.choice_db(name)
		self.users_col = self.db["Users"]
		self.offers_col = self.db["Offers"]

	def choice_db(self, db_name):
		self.db = self.client[db_name]

	def jsonDecode(self, jsonb):
		jsonStr = jsonb.decode('utf8').replace("'", '"')
		return(json.loads(jsonStr))

	def user_auth(self, jsonbUser):
		jsonUser = self.jsonDecode(jsonbUser)
		users = self.users_col.find()
		for user in users:
			if jsonUser["username"] == user["username"] and jsonUser["password"] == user["password"]:
				return json.dumps({"user_id" : user["_id"]})
		return 401	

	def get_user(self, user_id):
		users = self.users_col.find()
		offers = self.offers_col.find({"user_id": int(user_id["user_id"][0])})

		offers_list = []
		for offer in offers:

			offers_list.append(offer)
		
		user_json = None
		for user in users:
			if user["_id"] == int(user_id["user_id"][0]):
				user_json = user

		if user_json == None:
			return 401

		return json.dumps({"user" : user_json, "offers" : offers_list})

	def add_user(self, jsonbUser):
		users = self.users_col.find()
		jsonUser = self.jsonDecode(jsonbUser)

		if self.users_col.count_documents({}) != 0:
			last = self.users_col.count_documents({}) - 1
			new_id = users[last]["_id"] + 1
			jsonUser["_id"] = new_id
		else:
			last = 1
			jsonUser["_id"] = last

		for user in users:
			if jsonUser["username"] == user["username"] or jsonUser["email"] == user["email"]:
				return 400
		try:
			self.users_col.insert_one(jsonUser).inserted_id
		except writeError:
			return 500

	def offer_create(self, jsonbOffer):
		offers = self.offers_col.find()
		jsonOffer = self.jsonDecode(jsonbOffer)

		if self.offers_col.count_documents({}) != 0:
			last = self.offers_col.count_documents({}) - 1
			new_id = offers[last]["_id"] + 1
			jsonOffer["_id"] = new_id
		else:
			last = 1
			jsonOffer["_id"] = last

		try:
			self.offers_col.insert_one(jsonOffer).inserted_id
		except writeError:
			return 500

	def get_offer(self, jsonbReq):
		jsonReq = self.jsonDecode(jsonbReq)
		if "user_id" in jsonReq:
			user_offers = []
			for offer in self.offers_col.find({"user_id" : jsonReq["user_id"]}):
				user_offers.append(offer)
			return user_offers
		if "offer_id" in jsonReq:
			for offer in self.offers_col.find({"_id" : jsonReq["offer_id"]}):
				return offer
