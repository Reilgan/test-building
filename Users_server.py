from sanic import Sanic
from sanic.response import json
from sanic_validation import validate_json
from sanic_transmute import describe
import db_api

db = db_api.DataBase(name = "mydb")

users = Sanic(name = "Users")

schema_user_reg = {  
			"username": {'type': 'string'},
    		"password": {'type': 'string'},
    		"email"	  : {'type': 'string'},
    	 }

schema_user_auth = {
		    "username": {'type': 'string'},
			"password": {'type': 'string'}		
		 }

# @describe()
@users.route(methods = ["POST"], uri = '/user/registry')
@validate_json(schema_user_reg)
async def users_registry(request):
	error = db.add_user(request.body)
	if error == 400:
		return json({"massage" : "This username already exists"},
				 status = 400)
	if error == 500:
		return json({"massage" : "Failed to add user"},
				 status = 500)

	return json({"massage" : "Adding a new user was successful"},
				 status = 201)

@users.route(methods = ["POST"], uri = '/user/auth')
@validate_json(schema_user_auth)
async def users_auth(request):
	user_id = db.user_auth(request.body)
	if user_id == 401:
		return json({"The username or password you entered is incorrect"},
		 			 status = 401)
	return json(user_id, status = 201)

@users.route(methods = ["GET"], uri = '/user')
async def get_user(request):
	info = db.get_user(request.args)
	if info == 401:
		return json({"user not found"},
		 			 status = 401)
	return json(info, status = 200)

if __name__ == "__main__":
  users.run(host="0.0.0.0", port=8000)
