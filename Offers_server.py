from sanic import Sanic
from sanic.response import json
from sanic_validation import validate_json
import db_api

db = db_api.DataBase(name = "mydb")

Offers = Sanic(name = "Offers")

schema_offer_create = {  
				"user_id": {"type": "integer"},
				"title"  : {"type": "string"},
				"text"   : {"type": "string"}
    	 		}

@Offers.route(methods = ["POST"], uri = '/offer/create')
@validate_json(schema_offer_create)
async def offer_create(request):
	error = db.offer_create(request.body)
	if error == 500:
		return json({"massage" : "Failed to add offer"},
				 status = 500)
	return json({"massage" : "Adding a new offer was successful"},
				 status = 201)

@Offers.route(methods = ["POST"], uri = '/offer')
async def get_offer(request):
	info = db.get_offer(request.body)
	return json(info, status = 200)

if __name__ == "__main__":
  Offers.run(host="0.0.0.0", port=8001)